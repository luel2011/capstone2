let addCourseForm = document.querySelector("#createCourse")
console.log(addCourseForm)

addCourseForm.addEventListener("submit", (e) =>{
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let coursePrice = document.querySelector("#coursePrice").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	
	if((courseName !== '') && (courseDescription !== '') && (coursePrice !== '')){
		console.log(localStorage.token)
		fetch('https://warm-gorge-91771.herokuapp.com/api/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.token}`
			},
			body: JSON.stringify({
				"name": courseName,
				"price": coursePrice,
				"description": courseDescription
			})
		})
		.then(res => {
			return res.json()
		})
		.then(course_response => {
			console.log('response');
			console.log(course_response)
			
			if (course_response === true){
				alert("Course created succesfully!")
				window.location.replace("./courses.html")
			}else {
				alert("Something went wrong!")
			}

		})
	}else {
		alert("Forms must be filled out")
	}

});