
let registerForm = document.querySelector("#registerUser")
console.log(registerForm)

registerForm.addEventListener("submit", (e) =>{
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	let registerUser = document.querySelector("#registerUser");

	//validation to enable submit button when all the fields are populated and both passwords match
	//mobile number is 11
	// if (mobileNumber.value.length !== 11) {
	// 	window.alert("Please enter your number.")
	// 	mobileNumber.focus();
	// 	return false;
	// }

	if((password1 !== '' && password2 !=='') && (password1 === password2) && (mobileNumber.length === 11)){
		// check the database
		//check for duplicate email in the database
		//url where we can get the duplicate routes in our server
		//asynchronously load contentes of the url
		fetch('https://warm-gorge-91771.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"email": userEmail
			})
		})
		//returns a promise that resolves when the res is loaded
		// .then(result => res.json())//call this function when res is loaded
		.then(res => {
				return res.json()
			})
		.then(data =>{

			if (data === false) {// checks if email already exist
				fetch('https://warm-gorge-91771.herokuapp.com/api/users/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					"firstName": firstName,
					"lastName": lastName,
					"email": userEmail,
					"password": password1,
					"mobileNo": mobileNumber
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				console.log(data)
				//if registration is succesful
				if(data === true){
					alert("Registered Succesfully!")
					//redirect to login page
					window.location.replace("./login.html")

				}else{
				//error occured in registration
					alert("Something went wrong")
				}
			})

			}else {
				alert("Email already exist.")
			}
			//if no duplicates found
			//get the routes for registration in oure server
			

		})
	}else{
		alert("Sadd, Try Again")
	}
	

});