let loginForm = document.querySelector("#logInUser")
console.log(loginForm)

loginForm.addEventListener("submit", (e) =>{//prevents the default behavior of the forms
	e.preventDefault()

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value

	if(email === "" || password === ""){
		alert("Please input your email and/or password")
	}else {


		fetch('https://warm-gorge-91771.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
				return res.json()
		})
		.then(data => {
			console.log(data);
			if(data.accessToken){
				//succesful aithentcation winn return JSON web token
				localStorage.setItem('token', data.accessToken)

				fetch('https://warm-gorge-91771.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res =>{
					return res.json()
				})

				.then(user_response => {
					// console.log(user_response);
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")

				})

			}else {
				alert("Authentication Failed")
			}
		})

	}
})

